#!/bin/bash

# $ xsetwacom list
# Wacom Intuos PT M Pen stylus    	id: 11	type: STYLUS    
# Wacom Intuos PT M Pen eraser    	id: 12	type: ERASER    
# Wacom Intuos PT M Finger touch  	id: 16	type: TOUCH     
# Wacom Intuos PT M Finger pad    	id: 17	type: PAD      


# To activate touch controls again, simply uncomment following line
# xsetwacom set "Wacom Intuos PT M Finger touch" Gesture on

# Defining the Buttons 1-4 on the pad. With this config the first button presses 1 on your keyboard.
# Note that the first Button is "3" not "1", etc. 
xsetwacom set "Wacom Intuos PT M Finger pad" Button 3 "key 1"
xsetwacom set "Wacom Intuos PT M Finger pad" Button 1 "key 2"
xsetwacom set "Wacom Intuos PT M Finger pad" Button 9 "key 3"
xsetwacom set "Wacom Intuos PT M Finger pad" Button 8 "key 4"

# to change the buttons on the stylus, use the following:
#xsetwacom set "Wacom Intuos PT M Pen stylus" Button 1 "button 1+"
#xsetwacom set "Wacom Intuos PT M Pen stylus" Button 2 "button 2+"
#xsetwacom set "Wacom Intuos PT M Pen stylus" Button 3 "button 3+"
