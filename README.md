# Tablet

All our scripts and software we use with tablets can be found here.

## Wacom Intuos Pen & Touch
Here you find the actual version of our config file for ubuntu / mint 14.04 for Wacom Intuos Pen & Touch or Wacom Bamboo Pen & Touch devices.

Simply click on download zip and unzip everything for instance to your Downloads directory.

To set up the basic config, copy 52-wacom-options in the xorg.conf.d folder

```sh
# under ubuntu 14.04 or mint 17
sudo install -D ~/Downloads/tablet.git/xorg.conf.d/52-wacom-options.conf /usr/share/X11/xorg.conf.d/
```

Additionally you'll want to use the wacom.setup.sh to change your default key mapping.

Since you have to do use it every time the usb is plugged in again, you should copy it to your homedirectory.

```sh
# copy the wacom.setup.sh to your home
cp ~/Downloads/tablet.git/wacom.setup.sh ~/Desktop
```


## mypaint config

If you wish to use our config of mypaint, with colors already stored, please add the content of the folder mypaint to ~/.mypaint.
If you have never run the program mypaint before, run it once so that mypaint can create the folder.

```sh
# copying colors
cp -r ~/Downloads/tablet.git/mypaint/brushes ~/.mypaint/

#copying standard button shortcuts
cp ~/Downloads/tablet.git/mypaint/accelmap.conf ~/.mypaint/

#copying other settings (like brush stroke)
cp ~/Downloads/tablet.git/mypaint/settings.json ~/.mypaint/
```

Please check first if the folder exist. Current Beta version of mypaint have moved the config path to ~/.config/mypaint
