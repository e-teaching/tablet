## Wacom Intuos Pen & Touch
Here you find the actual version of our config file for ubuntu / mint 14.04 for Wacom Intuos Pen & Touch or Wacom Bamboo Pen & Touch devices.

Simply click on download zip and unzip everything for instance to your Downloads directory.

To set up the basic config, copy 52-wacom-options in the xorg.conf.d folder

```sh
# under ubuntu 14.04 or mint 17
sudo install -D ~/Downloads/tablet.git/xorg.conf.d/52-wacom-options.conf /usr/share/X11/xorg.conf.d/
```

Additionally you'll want to use the wacom.setup.sh to change your default key mapping.
