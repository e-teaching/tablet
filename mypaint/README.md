## mypaint config

If you wish to use our config of mypaint, with colors already stored, please add the content of the folder mypaint to ~/.mypaint.
If you have never run the program mypaint before, run it once so that mypaint can create the folder.

```sh
# copying colors
cp -r ./mypaint/brushes ~/.mypaint/

#copying standard button shortcuts
cp ./mypaint/accelmap.conf ~/.mypaint/

#copying other settings (like brush stroke)
cp ./mypaint/settings.json ~/.mypaint/
```

Please check first if the folder exist. Current Beta version of mypaint have moved the config path to ~/.config/mypaint
